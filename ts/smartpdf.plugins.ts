// native
import * as http from 'http';
import * as path from 'path';

export { http, path };

// @pushrocks
import * as smartfile from '@pushrocks/smartfile';
import * as smartdelay from '@pushrocks/smartdelay';
import * as smartpromise from '@pushrocks/smartpromise';
import * as smartpath from '@pushrocks/smartpath';
import * as smartpuppeteer from '@pushrocks/smartpuppeteer';
import * as smartnetwork from '@pushrocks/smartnetwork';
import * as smartunique from '@pushrocks/smartunique';

export {
  smartfile,
  smartdelay,
  smartpromise,
  smartpath,
  smartpuppeteer,
  smartunique,
  smartnetwork,
};

// tsclass scope
import * as tsclass from '@tsclass/tsclass';

export { tsclass };

// thirdparty
import pdfMerger from 'pdf-merger-js';
// @ts-ignore
import pdf2json from 'pdf2json';
import express from 'express';

export { pdfMerger, pdf2json, express };
